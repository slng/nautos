import React, { Component } from 'react';
import proptypes from 'prop-types';
import ReactDOM from 'react-dom';
import ApolloClient from 'apollo-boost';
import { ApolloProvider, Query } from 'react-apollo';
import gql from 'graphql-tag';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import * as fontAwesomeIcons from '@fortawesome/fontawesome-free-solid';
import ReactMarkdown from 'react-markdown';
import { request } from 'graphql-request';
import moment from 'moment';

import './index.css';

class Nautos extends Component {
  logMutation = `mutation createEntry(
      $AppName: String!, 
      $Operation: String!,
      $Object: String!,
      $user: String!,
      $valueString: String!,
      $objectString: String!
    ){
      createEntry(
        AppName:$AppName,
        Operation:$Operation,
        Object:$Object,
        user:$user,
        valueString:$valueString,
        objectString:$objectString
      ){
        id
      }
    }`;

  logQuery = `query searchForField(
      $AppName: String!,
      $Object: String!,
      $value: String!,
      $field: String!,
      $Operation: String
    ){
     searchForField(
       AppName: $AppName,
       Object: $Object,
       value: $value,
       field: $field,
       Operation: $Operation
      ){
       timestamp
       valueString
      }
    }`;

  messagesQuery = `query messages(
    $appName: String!
    $appPath: String!
    $customKey: String!
    $customValue: String!
  ) {
    messages(
      orderBy: updatedAt_DESC
      where: {
        appName: $appName
        appPath: $appPath
        customKey: $customKey
        customValue: $customValue
      }
    ) {
      id
      message
      appName
      appPath
      createdAt
      updatedAt
      customKey
      customValue
      btnSecondaryLabel
      btnSecondaryUrl
      btnPrimaryLabel
      btnPrimaryUrl
      operators
      breakOff
      inTime
    }
  }`;

  constructor(props) {
    super();
    this.client = new ApolloClient({
      uri: props.url
    });

    let requestUrl = '';
    if (window.location.href.indexOf('http://tabea.pewo.int') > -1) {
      requestUrl = props.lokiUrl;
    } else {
      requestUrl = 'http://tools.pim.pewo.int:48001';
    }

    this.state = {
      requestUrl,
      logData: null
    };
    request(requestUrl, this.logQuery, {
      AppName: 'nautos',
      Object: 'click',
      Operation: 'dismiss',
      value: `"${props.toLog}"`,
      field: 'toLog'
    }).then(response => {
      this.setState({ logData: response.searchForField });
    });
  }

  log(message, isOk) {
    if (this.props.user) {
      const variables = {
        AppName: 'nautos',
        Operation: isOk ? 'accept' : 'dismiss',
        Object: 'click',
        user: this.props.user,
        valueString: JSON.stringify(
          Object.assign({}, message, { toLog: this.props.toLog })
        ),
        objectString: JSON.stringify({})
      };
      request(this.state.requestUrl, this.logMutation, variables).catch(err =>
        console.error(err)
      );
    }
  }

  goto(message, key) {
    this.log(message, key.indexOf('btnPrimaryUrl') > -1);
    const url = message[key];
    if (url) {
      window.location.href = url.indexOf('http') > -1 ? url : `http://${url}`;
    }
  }

  getPlaceHolder() {
    const { id, className } = this.props;
    return <div id={id} className={className} />;
  }

  render() {
    const {
      id,
      className,
      appName,
      appPath,
      customKey,
      customValue,
      templateData
    } = this.props;
    const { logData } = this.state;
    if (!logData) {
      return this.getPlaceHolder();
    }
    return (
      <ApolloProvider client={this.client}>
        <Query
          query={gql`
            ${this.messagesQuery}
          `}
          variables={{
            appName,
            appPath,
            customKey,
            customValue: customValue || ''
          }}>
          {({ loading, error, data }) => {
            if (loading) return this.getPlaceHolder();
            if (error) return this.getPlaceHolder();
            const { messages } = data;
            if (messages.length === 0) {
              return this.getPlaceHolder();
            }
            const [message] = messages;

            const now = moment();
            const dismisses = logData.filter(log => {
              const logCreationDate = moment(log.timestamp);
              const diff = now.diff(logCreationDate, 'hours', true);
              return diff <= message.inTime;
            }).length;
            if (dismisses > message.breakOff) {
              return this.getPlaceHolder();
            }

            templateData.forEach(template => {
              const reg = new RegExp(template.pattern, 'g');
              message.message = message.message.replace(reg, template.replace);
            });

            const btnClass = url =>
              url ? 'col-md-5 btn' : 'col-md-5 btn disabled';
            return (
              <div id={id} className={`${className} position-relative`}>
                <FontAwesomeIcon
                  className="icon"
                  icon={fontAwesomeIcons[this.props.icon]}
                />
                <div className="content">
                  <div className="message">
                    <ReactMarkdown source={message.message} />
                  </div>
                  <div className="btns">
                    <div
                      className={`${btnClass(
                        message.btnSecondaryUrl
                      )} btn-secondary `}
                      onClick={this.goto.bind(
                        this,
                        message,
                        'btnSecondaryUrl'
                      )}>
                      {message.btnSecondaryLabel}
                    </div>
                    <div
                      className={`${btnClass(
                        message.btnPrimaryUrl
                      )} btn-primary offset-md-2`}
                      onClick={this.goto.bind(this, message, 'btnPrimaryUrl')}>
                      {message.btnPrimaryLabel}
                    </div>
                  </div>
                </div>
              </div>
            );
          }}
        </Query>
      </ApolloProvider>
    );
  }
}

Nautos.defaultProps = {
  id: window.location.toString(),
  className: '',
  appName: '',
  appPath: '',
  customKey: '',
  customValue: '',
  user: '',
  templateData: [],
  toLog: '',
  url: '',
  lokiUrl: '',
  icon: 'faUserAstronaut'
};

Nautos.propTypes = {
  id: proptypes.string,
  className: proptypes.string,
  appName: proptypes.string,
  appPath: proptypes.string,
  customKey: proptypes.string,
  customValue: proptypes.string,
  user: proptypes.string,
  toLog: proptypes.string,
  templateData: proptypes.array,
  icon: proptypes.string,
  url: proptypes.string,
  lokiUrl: proptypes.string
};

// ReactDOM.render(
//   <Nautos
//     appName={'tabea'}
//     appPath={''}
//     customKey={'net'}
//     customValue={'0'}
//     className="new-col-md-2"
//     templateData={[
//       {
//         pattern: '{username}',
//         replace: 'pim_super'
//       }
//     ]}
//     toLog={'5'}
//     user={'test_pim_super_'}
//     lokiUrl={'http://tools.pim.pewo.int:8001'}
//     url={'https://api-euwest.graphcms.com/v1/cjlxmoj8j000a01dedli4lcq8/master'}
//   />,
//   document.getElementById('root')
// );
// ReactDOM.render(<Nautos />, document.getElementById('root'));

export default Nautos;
