var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _templateObject = _taggedTemplateLiteral(['\n            ', '\n          '], ['\n            ', '\n          ']);

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import proptypes from 'prop-types';
import ReactDOM from 'react-dom';
import ApolloClient from 'apollo-boost';
import { ApolloProvider, Query } from 'react-apollo';
import gql from 'graphql-tag';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import * as fontAwesomeIcons from '@fortawesome/fontawesome-free-solid';
import ReactMarkdown from 'react-markdown';
import { request } from 'graphql-request';
import moment from 'moment';

import './index.css';

var Nautos = function (_Component) {
  _inherits(Nautos, _Component);

  function Nautos(props) {
    _classCallCheck(this, Nautos);

    var _this = _possibleConstructorReturn(this, (Nautos.__proto__ || Object.getPrototypeOf(Nautos)).call(this));

    _this.logMutation = 'mutation createEntry(\n      $AppName: String!, \n      $Operation: String!,\n      $Object: String!,\n      $user: String!,\n      $valueString: String!,\n      $objectString: String!\n    ){\n      createEntry(\n        AppName:$AppName,\n        Operation:$Operation,\n        Object:$Object,\n        user:$user,\n        valueString:$valueString,\n        objectString:$objectString\n      ){\n        id\n      }\n    }';
    _this.logQuery = 'query searchForField(\n      $AppName: String!,\n      $Object: String!,\n      $value: String!,\n      $field: String!,\n      $Operation: String\n    ){\n     searchForField(\n       AppName: $AppName,\n       Object: $Object,\n       value: $value,\n       field: $field,\n       Operation: $Operation\n      ){\n       timestamp\n       valueString\n      }\n    }';
    _this.messagesQuery = 'query messages(\n    $appName: String!\n    $appPath: String!\n    $customKey: String!\n    $customValue: String!\n  ) {\n    messages(\n      orderBy: updatedAt_DESC\n      where: {\n        appName: $appName\n        appPath: $appPath\n        customKey: $customKey\n        customValue: $customValue\n      }\n    ) {\n      id\n      message\n      appName\n      appPath\n      createdAt\n      updatedAt\n      customKey\n      customValue\n      btnSecondaryLabel\n      btnSecondaryUrl\n      btnPrimaryLabel\n      btnPrimaryUrl\n      operators\n      breakOff\n      inTime\n    }\n  }';

    _this.client = new ApolloClient({
      uri: props.url
    });

    var requestUrl = '';
    if (window.location.host === 'tabea.pewo.int') {
      requestUrl = props.lokiUrl;
    } else {
      requestUrl = 'http://tools.pim.pewo.int:48001';
    }

    _this.state = {
      requestUrl: requestUrl,
      logData: null
    };
    request(requestUrl, _this.logQuery, {
      AppName: 'nautos',
      Object: 'click',
      Operation: 'dismiss',
      value: '"' + props.toLog + '"',
      field: 'toLog'
    }).then(function (response) {
      _this.setState({ logData: response.searchForField });
    });
    return _this;
  }

  _createClass(Nautos, [{
    key: 'log',
    value: function log(message, isOk) {
      if (this.props.user) {
        var variables = {
          AppName: 'nautos',
          Operation: isOk ? 'accept' : 'dismiss',
          Object: 'click',
          user: this.props.user,
          valueString: JSON.stringify(Object.assign({}, message, { toLog: this.props.toLog })),
          objectString: JSON.stringify({})
        };
        request(this.state.requestUrl, this.logMutation, variables).catch(function (err) {
          return console.error(err);
        });
      }
    }
  }, {
    key: 'goto',
    value: function goto(message, key) {
      this.log(message, key.indexOf('btnPrimaryUrl') > -1);
      var url = message[key];
      if (url) {
        window.location.href = url.indexOf('http') > -1 ? url : 'http://' + url;
      }
    }
  }, {
    key: 'getPlaceHolder',
    value: function getPlaceHolder() {
      var _props = this.props,
          id = _props.id,
          className = _props.className;

      return React.createElement('div', { id: id, className: className });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props2 = this.props,
          id = _props2.id,
          className = _props2.className,
          appName = _props2.appName,
          appPath = _props2.appPath,
          customKey = _props2.customKey,
          customValue = _props2.customValue,
          templateData = _props2.templateData;
      var logData = this.state.logData;

      if (!logData) {
        return this.getPlaceHolder();
      }
      return React.createElement(
        ApolloProvider,
        { client: this.client },
        React.createElement(
          Query,
          {
            query: gql(_templateObject, this.messagesQuery),
            variables: {
              appName: appName,
              appPath: appPath,
              customKey: customKey,
              customValue: customValue || ''
            } },
          function (_ref) {
            var loading = _ref.loading,
                error = _ref.error,
                data = _ref.data;

            if (loading) return _this2.getPlaceHolder();
            if (error) return _this2.getPlaceHolder();
            var messages = data.messages;

            if (messages.length === 0) {
              return _this2.getPlaceHolder();
            }

            var _messages = _slicedToArray(messages, 1),
                message = _messages[0];

            var now = moment();
            var dismisses = logData.filter(function (log) {
              var logCreationDate = moment(log.timestamp);
              var diff = now.diff(logCreationDate, 'hours', true);
              return diff <= message.inTime;
            }).length;
            if (dismisses > message.breakOff) {
              return _this2.getPlaceHolder();
            }

            templateData.forEach(function (template) {
              var reg = new RegExp(template.pattern, 'g');
              message.message = message.message.replace(reg, template.replace);
            });

            var btnClass = function btnClass(url) {
              return url ? 'col-md-5 btn' : 'col-md-5 btn disabled';
            };
            return React.createElement(
              'div',
              { id: id, className: className + ' position-relative' },
              React.createElement(FontAwesomeIcon, {
                className: 'icon',
                icon: fontAwesomeIcons[_this2.props.icon]
              }),
              React.createElement(
                'div',
                { className: 'content' },
                React.createElement(
                  'div',
                  { className: 'message' },
                  React.createElement(ReactMarkdown, { source: message.message })
                ),
                React.createElement(
                  'div',
                  { className: 'btns' },
                  React.createElement(
                    'div',
                    {
                      className: btnClass(message.btnSecondaryUrl) + ' btn-secondary ',
                      onClick: _this2.goto.bind(_this2, message, 'btnSecondaryUrl') },
                    message.btnSecondaryLabel
                  ),
                  React.createElement(
                    'div',
                    {
                      className: btnClass(message.btnPrimaryUrl) + ' btn-primary offset-md-2',
                      onClick: _this2.goto.bind(_this2, message, 'btnPrimaryUrl') },
                    message.btnPrimaryLabel
                  )
                )
              )
            );
          }
        )
      );
    }
  }]);

  return Nautos;
}(Component);

Nautos.defaultProps = {
  id: window.location.toString(),
  className: '',
  appName: '',
  appPath: '',
  customKey: '',
  customValue: '',
  user: '',
  templateData: [],
  toLog: '',
  url: '',
  lokiUrl: '',
  icon: 'faUserAstronaut'
};

Nautos.propTypes = {
  id: proptypes.string,
  className: proptypes.string,
  appName: proptypes.string,
  appPath: proptypes.string,
  customKey: proptypes.string,
  customValue: proptypes.string,
  user: proptypes.string,
  toLog: proptypes.string,
  templateData: proptypes.array,
  icon: proptypes.string,
  url: proptypes.string,
  lokiUrl: proptypes.string
};

// ReactDOM.render(
//   <Nautos
//     appName={'tabea'}
//     appPath={''}
//     customKey={'net'}
//     customValue={'0'}
//     className="new-col-md-2"
//     templateData={[
//       {
//         pattern: '{username}',
//         replace: 'pim_super'
//       }
//     ]}
//     toLog={'5'}
//     user={'test_pim_super_'}
//     lokiUrl={'http://tools.pim.pewo.int:8001'}
//     url={'https://api-euwest.graphcms.com/v1/cjlxmoj8j000a01dedli4lcq8/master'}
//   />,
//   document.getElementById('root')
// );
// ReactDOM.render(<Nautos />, document.getElementById('root'));

export default Nautos;